import React, { Component } from 'react';

export class Button extends Component {
  render() {
    return (
      <button className="App">
        {this.props.text}  
      </button>
    );    
  }
}